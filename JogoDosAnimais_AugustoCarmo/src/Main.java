/*
    Author: Augusto Ferreira Andrade do Carmo
    Email: augusto.carmo.1992@gmail.com
    Phone: +55 31 99213-8796
 */

import game.Game;
import models.Animal;
import models.Characteristic;
import views.GameView;
import views.IGameView;
import views.IWindowView;
import views.WindowView;

/**
 Creation Date: 06/11/2015 <dd/MM/yyyy>
 Author: Augusto Ferreira Andrade do Carmo
 Email: augusto.carmo.1992@gmail.com
 Phone: +55 31 99213-8796
 */
public class Main {
    public static void main(String[] args) {

        // Create the Main window.
        IWindowView windowView = new WindowView();
        windowView.initialize();

        // Create the game view
        IGameView gameView = new GameView();
        gameView.initialize();

        // Set game view as the content view of the main window
        windowView.setContentView(gameView);

        // Show the view to the player. Let the game begins :).
        windowView.showView();


    }
}
