package presenters;

import views.IGameView;

/**
    Creation Date: 06/11/2015 <dd/MM/yyyy>
    Author: Augusto Ferreira Andrade do Carmo
    Email: augusto.carmo.1992@gmail.com
    Phone: +55 31 99213-8796
 */
public interface IGamePresenter {

    IGameView getGameView();
    void setGameView(IGameView gameView);
    void initialize();
    void newGuess();
}
