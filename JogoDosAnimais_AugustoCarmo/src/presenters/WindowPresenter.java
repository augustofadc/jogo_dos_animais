package presenters;

import views.IWindowView;

/**
    Creation Date: 06/11/2015 <dd/MM/yyyy>
    Author: Augusto Ferreira Andrade do Carmo
    Email: augusto.carmo.1992@gmail.com
    Phone: +55 31 99213-8796
 */
public class WindowPresenter implements IWindowPresenter {

    IWindowView mWindowView;

    @Override
    public IWindowView getWindowView() {
        return mWindowView;
    }

    @Override
    public void setWindowView(IWindowView windowView) {
        mWindowView = windowView;
    }

    @Override
    public void setContentPane() {

    }
}
