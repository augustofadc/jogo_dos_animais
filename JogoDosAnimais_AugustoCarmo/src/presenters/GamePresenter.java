package presenters;

import game.Game;
import views.IGameView;

/**
    Creation Date: 06/11/2015 <dd/MM/yyyy>
    Author: Augusto Ferreira Andrade do Carmo
    Email: augusto.carmo.1992@gmail.com
    Phone: +55 31 99213-8796
 */
public class GamePresenter implements IGamePresenter {


    private Game mGame;
    private IGameView mGameView;

    public GamePresenter() {
        mGame = new Game();
    }

    @Override
    public IGameView getGameView() {
        return mGameView;
    }

    @Override
    public void setGameView(IGameView gameView) {
        mGameView = gameView;
    }

    @Override
    public void initialize() {
    }

    @Override
    public void newGuess() {
        mGame.newGuess();
    }
}
