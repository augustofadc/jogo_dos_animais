package tree;

import com.sun.istack.internal.NotNull;
import models.IAnimal;
import models.ICharacteristic;

/**
 Creation Date: 06/11/2015 <dd/MM/yyyy>
 Author: Augusto Ferreira Andrade do Carmo
 Email: augusto.carmo.1992@gmail.com
 Phone: +55 31 99213-8796
 */
public class HaveOrNotTreeNode {
    private HaveOrNotTreeNode mParent;
    private HaveOrNotTreeNode mHave;
    private HaveOrNotTreeNode mHaveNot;


    /**
     * A tree node holds two information:<br>
     * (1) An animal characteristic;<br>
     * (2) An animal itself.
     */
    private ICharacteristic mCharacteristic;
    private IAnimal mAnimal;

    /**
     * The default constructor the receive not null parameters.
     * @param characteristic
     * @param animal
     */
    protected HaveOrNotTreeNode(@NotNull ICharacteristic characteristic, @NotNull IAnimal animal) {
        mParent = null;
        mCharacteristic = characteristic;
        mAnimal = animal;
        mHave = null;
        mHaveNot = null;
    }

    /**
     * It will be a leaf if the node does not have any children (they are set null).<br>
     * The possible children are:<br>
     * <b>(1)</b> Have tree node;<br>
     * <b>(2)</b> Have not tree node.
     * @return
     */
    public Boolean isLeaf(){
        return mHave == null && mHaveNot == null;
    }

    /**
     * It will be a branch if it is not a leaf
     * @return
     */
    public Boolean isBranch(){
        return !isLeaf();
    }

    /**
     * A new Tree Node childre has two places to be added:<br>
     * <b>(1)</b> have tree node, if it has the characteristics of this node;<br>
     * <b>(1)</b> have not tree node, if it does not have the characteristics of this node.<br>
     * @param have
     * @param characteristic
     * @param animal
     */
    public void add(Boolean have, @NotNull ICharacteristic characteristic,@NotNull IAnimal animal){
        if(have){
            HaveOrNotTreeNode haveTreeNode = new HaveOrNotTreeNode(characteristic, animal);
            mHave = haveTreeNode;
            haveTreeNode.setParent(this);
        }else{
            HaveOrNotTreeNode haveNotTreeNode = new HaveOrNotTreeNode(characteristic, animal);
            mHaveNot = haveNotTreeNode;
            haveNotTreeNode.setParent(this);
        }
    }

    public ICharacteristic getCharacteristic(){
        return mCharacteristic;
    }

    public IAnimal getAnimal(){
        return mAnimal;
    }

    public HaveOrNotTreeNode getParent(){
        return mParent;
    }

    protected void setParent(@NotNull HaveOrNotTreeNode haveOrNotTreeNode){
        mParent = haveOrNotTreeNode;
    }

    public HaveOrNotTreeNode getHaveTreeNode(){
        return mHave;
    }

    public HaveOrNotTreeNode getHaveNotTreeNode(){
        return mHaveNot;
    }

    /**
     * This method returns the possible animal, if it's not this one.<br>
     * The possible animal is contained in the <b>first</b> <i>haveTreeNode</i> of its parent <br>
     * <br>
     * This method will return <b>null</b> if the condition above is not satisfied.
     * @return
     */
    public IAnimal getPossibleAnimal(){
        IAnimal possibleAnimal = null;

        HaveOrNotTreeNode currentTreeNode = this;
        HaveOrNotTreeNode currentTreeNodeParent = currentTreeNode.getParent();
        /**
         * keepLooking is used to check if the condition is satisfied.<br>
         * While its value is equals to false, it is not.
         */
        boolean keepLooking = true;

        /*
         Run through currentTreeNode parent until the condition is satisfied.
         */
        while(  currentTreeNode != null &&
                currentTreeNodeParent != null &&
                keepLooking){

            if(currentTreeNodeParent.getHaveTreeNode() != null &&
                    currentTreeNodeParent.getHaveTreeNode().equals(currentTreeNode)){
                //Possible animal found!
                possibleAnimal = currentTreeNodeParent.getAnimal();
                keepLooking = false;
            }else {
                currentTreeNode = currentTreeNodeParent;
                currentTreeNodeParent = currentTreeNodeParent.getParent();
            }
        }

        return possibleAnimal;
    }
}
