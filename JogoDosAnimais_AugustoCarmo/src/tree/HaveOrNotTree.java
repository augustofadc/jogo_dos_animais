package tree;

import com.sun.istack.internal.NotNull;
import models.IAnimal;
import models.ICharacteristic;

/**
    Creation Date: 06/11/2015 <dd/MM/yyyy>
    Author: Augusto Ferreira Andrade do Carmo
    Email: augusto.carmo.1992@gmail.com
    Phone: +55 31 99213-8796
 */
public class HaveOrNotTree {

    /**
     * The root element is ALWAYS a characteristic. It can never be an animal.
     */
    private HaveOrNotTreeNode mRoot;

    /**
     *
     * @param characteristic Can't be null, and it will be a characteristic that the animal has.
     * @param animal Can't be null.
     */

    public HaveOrNotTree(@NotNull ICharacteristic characteristic,@NotNull IAnimal animal) {
        mRoot = new HaveOrNotTreeNode(characteristic, animal);

    }

    public HaveOrNotTreeNode getTreeNode(){
        return mRoot;
    }
}
