package views;

import presenters.GamePresenter;
import presenters.IGamePresenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 Creation Date: 06/11/2015 <dd/MM/yyyy>
 Author: Augusto Ferreira Andrade do Carmo
 Email: augusto.carmo.1992@gmail.com
 Phone: +55 31 99213-8796
 */
public class GameView extends JPanel implements IGameView{

    private IGamePresenter mGamePresenter;

    //GUI
    private JLabel mMenuTitleLabel;
    private JButton mMenuOkButton;

    public GameView(){
        super();
        setGamePresenter(new GamePresenter());
        mGamePresenter.setGameView(this);

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        /**
         * Box is used to centralize vertically the elements
         * that's the purpose of box.add(Box.createVerticalGlue());
         */
        Box box = new Box(BoxLayout.Y_AXIS);
        add( box );

        box.add(Box.createVerticalGlue());

        mMenuTitleLabel = new JLabel("Pense em um animal");
        mMenuTitleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        box.add(mMenuTitleLabel);

        //Create a margin in Y axis
        box.add(Box.createRigidArea(new Dimension(0,10)));

        mMenuOkButton = new JButton("Ok");
        mMenuOkButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        //adding a click listener to the ok button
        mMenuOkButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Load the Playing State
                mGamePresenter.newGuess();
                //Than, return the focus to the OK button
                mMenuOkButton.requestFocus();
            }
        });

        box.add(mMenuOkButton);

        box.add(Box.createVerticalGlue());
    }

    @Override
    public Container getContainer() {
        return this;
    }

    @Override
    public IGamePresenter getGamePresenter() {
        return mGamePresenter;
    }

    @Override
    public void setGamePresenter(IGamePresenter gamePresenter) {
        mGamePresenter = gamePresenter;
    }

    @Override
    public void initialize() {
        mGamePresenter.initialize();
    }
}
