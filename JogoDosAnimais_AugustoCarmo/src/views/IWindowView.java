package views;

import presenters.IWindowPresenter;

/**
 Creation Date: 06/11/2015 <dd/MM/yyyy>
 Author: Augusto Ferreira Andrade do Carmo
 Email: augusto.carmo.1992@gmail.com
 Phone: +55 31 99213-8796
 */
public interface IWindowView extends IView{

    IWindowPresenter getWindowPresenter();
    void setWindowPresenter(IWindowPresenter windowPresenter);
    void initialize();
    void showView();
    void setContentView(IView view);
}
