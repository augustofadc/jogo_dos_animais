package views;

import presenters.IWindowPresenter;
import presenters.WindowPresenter;

import javax.swing.*;
import java.awt.*;

/**
 Creation Date: 06/11/2015 <dd/MM/yyyy>
 Author: Augusto Ferreira Andrade do Carmo
 Email: augusto.carmo.1992@gmail.com
 Phone: +55 31 99213-8796
 */
public class WindowView extends JFrame implements IWindowView {

    private static final String WINDOW_TITLE = "Jogo dos Animais por Augusto Carmo";
    private static final Integer VIEW_WIDTH = 400;
    private static final Integer VIEW_HEIGHT = 200;

    private IWindowPresenter mWindowPresenter;

    public WindowView (){
        super();
        setWindowPresenter(new WindowPresenter());
        mWindowPresenter.setWindowView(this);
    }

    @Override
    public Container getContainer() {
        return this;
    }

    @Override
    public IWindowPresenter getWindowPresenter() {
        return mWindowPresenter;
    }

    @Override
    public void initialize() {
        //Terminate program when this view is closed
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(VIEW_WIDTH, VIEW_HEIGHT);
        //Centralize
        setLocationRelativeTo(null);
        //Setting window minimum size
        setMinimumSize(new Dimension(VIEW_WIDTH, VIEW_HEIGHT));
        //Setting window title
        setTitle(WINDOW_TITLE);
    }

    @Override
    public void setWindowPresenter(IWindowPresenter windowPresenter) {
        mWindowPresenter = windowPresenter;
    }

    @Override
    public void showView() {
        pack();
        setVisible(Boolean.TRUE);
    }

    @Override
    public void setContentView(IView view) {
        this.setContentPane(view.getContainer());
    }
}
