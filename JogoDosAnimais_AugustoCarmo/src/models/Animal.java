package models;

/**
    Creation Date: 06/11/2015 <dd/MM/yyyy>
    Author: Augusto Ferreira Andrade do Carmo
    Email: augusto.carmo.1992@gmail.com
    Phone: +55 31 99213-8796
 */
public class Animal implements IAnimal {

    private String mName;

    public Animal() {
    }

    public Animal(String name) {
        setName(name);
    }

    @Override
    public String getName() {
        return mName;
    }

    @Override
    public void setName(String name) {
        mName = name;
    }
}
