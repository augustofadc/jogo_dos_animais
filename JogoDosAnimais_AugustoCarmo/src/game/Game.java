package game;

import models.Animal;
import models.Characteristic;
import models.IAnimal;
import models.ICharacteristic;
import tree.HaveOrNotTree;
import tree.HaveOrNotTreeNode;

import javax.swing.*;

/**
    Creation Date: 06/11/2015 <dd/MM/yyyy>
    Author: Augusto Ferreira Andrade do Carmo
    Email: augusto.carmo.1992@gmail.com
    Phone: +55 31 99213-8796
 */
public class Game {

    /**
     * This animal is used when there are no other animal to be guessed
     */
    private final IAnimal mMonkey = new Animal("Macaco");
    /**
     * This is the knowledge of the game. This variable will hold<br>
     * every single animal user entered and its characteristics.
     */
    private HaveOrNotTree mHaveOrNotTree;

    //Default Constructor
    public Game() {
        mHaveOrNotTree = new HaveOrNotTree(new Characteristic("vive na água"), new Animal("Tubarão"));
    }

    public void initialize(){

    }

    /**
     * This method holds the <b>main logic</b> of the game<br>
     * <br>
     * This method will interact with the player trying to guess which animal he is thinking<br>
     * There are two situations:<br>
     * <b>(1)</b> The game could successfully guess which animal the player is thinking<br>
     * <b>(2)</b> The game could not guess which animal the player is thinking<br>
     * <br>
     * In the case <b>(2)</b>, the user must input a new animal and one of its characteristics.<br>
     * which will be added to the Game knowledge (<b>tree</b>).
     */
    public void newGuess(){
        HaveOrNotTreeNode currentNode = mHaveOrNotTree.getTreeNode();

        /**
         * This variable is responsible to say whether there are more characteristics<br>
         * to be checked in the tree or not.
         */
        boolean keepCheckingCharacteristics = true;
        /**
         * This variable is responsible to say if the animal that the player is thinking <br>
         * has the last characteristic checked in the tree.<br>
         *
         */
        boolean haveCurrentCharacteristic = false;

        /*
            Go through the tree checking the characteristics until a leaf is reached
         */
        do {
            /**
             * This variable is responsible to say if the animal the player is thinking of<br>
             * has the characteristic presented in the current tree node.
             */
            Boolean hasCharacteristic = checkIfHasCharacteristic(currentNode.getCharacteristic());
            // if the value returned is null, return. It will not be a valid entry
            if(hasCharacteristic == null) return;

            if(hasCharacteristic){
                /*
                    If it has, check if the current node have a have node tree:
                    (Have): set it as the new current tree node
                    (Haven't): stop the loop
                 */
                if(currentNode.getHaveTreeNode() == null){
                    keepCheckingCharacteristics = false;
                    haveCurrentCharacteristic = true;
                }else{
                    currentNode = currentNode.getHaveTreeNode();
                }
            }else{
                /*
                    If it has, check if the current node have a have not node tree:
                    (Have): set it as the new current tree node
                    (Haven't): stop the loop
                 */
                if(currentNode.getHaveNotTreeNode() == null){
                    keepCheckingCharacteristics = false;
                    haveCurrentCharacteristic = false;
                }else{
                    currentNode = currentNode.getHaveNotTreeNode();
                }
            }
        }while (keepCheckingCharacteristics); //stop condition

        /**
         * This will be the possible animal that the player is thinking.
         */
        IAnimal possibleAnimal = null;
        if(haveCurrentCharacteristic){
            possibleAnimal = currentNode.getAnimal();
        }else{
            possibleAnimal = currentNode.getPossibleAnimal();
            /*
                If there is no possible animal, set monkey as being it.
             */
            if(possibleAnimal == null){
                possibleAnimal = mMonkey;
            }
        }

        /**
         * Asks with the player if the possible animal is the one he is thinking.
         */
        Boolean isTheAnimal = checkIfIsTheAnimal(possibleAnimal);
        // if the value returned is null, return. It will not be a valid entry
        if (isTheAnimal == null) return;

        if(isTheAnimal){
            // If yes, the game will display a "I got it again" message
            showRightGuessMessage();
        }else{
            // If not, the player will be asked to enter a animal and one of its characteristics
            String animalName = getGuessedAnimalNameFromUser();
            // If the animal name is null or empty, return. It will not be a valid entry
            if (animalName == null || animalName.trim().isEmpty()) return;
            String characteristic = getGuessedAnimalCharacteristicFromUser(animalName, possibleAnimal);
            // If the characterist name is null or empty, return. It will not be a valid entry
            if (characteristic == null || characteristic.trim().isEmpty()) return;

            // Add the new animal added by the player to the tree <Game Knowledge>
            currentNode.add(haveCurrentCharacteristic, new Characteristic(characteristic), new Animal(animalName));
        }
    }

    // GUI call functions

    private Boolean checkIfHasCharacteristic(ICharacteristic characteristic){
        String title = "Checando as Caracteristicas";
        String message = "O animal que você pensou " + characteristic.getName() + "?";
        Integer chosenOption = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
        switch (chosenOption){
            case JOptionPane.YES_OPTION:
                return Boolean.TRUE;
            case JOptionPane.NO_OPTION:
                return Boolean.FALSE;
            case JOptionPane.CANCEL_OPTION:
                return null;
            default:
                return null;
        }
    }

    private Boolean checkIfIsTheAnimal (IAnimal animal){
        String title = "Checando o possível Animal";
        String message = "O animal que você pensou é " + animal.getName() + "?";
        Integer chosenOption = JOptionPane.showConfirmDialog(null, message, title, JOptionPane.YES_NO_OPTION);
        switch (chosenOption){
            case JOptionPane.YES_OPTION:
                return Boolean.TRUE;
            case JOptionPane.NO_OPTION:
                return Boolean.FALSE;
            case JOptionPane.CANCEL_OPTION:
                return null;
            default:
                return null;
        }
    }

    private void showRightGuessMessage(){
        String message = "Acertei de novo!";
        JOptionPane.showMessageDialog(null, message);
    }

    private String getGuessedAnimalNameFromUser(){
        String title = "Qual o animal?";
        String message = "Qual o animal que você pensou?";
        return JOptionPane.showInputDialog(null, message, title, JOptionPane.OK_CANCEL_OPTION);
    }

    private String getGuessedAnimalCharacteristicFromUser(String rightAnimalName, IAnimal wrongAnimal){
        String title = "Qual o animal?";
        String message = "Um(a) " + rightAnimalName + "  _________ mas o " + wrongAnimal.getName() + " não.";
        return JOptionPane.showInputDialog(null, message, title, JOptionPane.OK_CANCEL_OPTION);
    }
}
